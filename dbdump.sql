-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : database
-- Généré le : ven. 20 août 2021 à 18:42
-- Version du serveur :  5.7.29
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `symfony`
--

-- --------------------------------------------------------

--
-- Structure de la table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `about`
--

INSERT INTO `about` (`id`, `about_detail`) VALUES
(1, 'Tous les domaines concernant l’informatique et la création multimédia en général me passionnent depuis mon plus jeune âge.\r\n<br>\r\n<br>\r\nInitialement pris de passion pour le Game Design et la création de jeux vidéos/applications/contenus vidéo, j’ai commencé tôt à me former en autodidacte et à effectuer une veille informationnelle continue sur toutes les nouvelles technologies et méthodes concernant l’audiovisuel.\r\n<br>\r\n<br>\r\nSouriant, agréable et volontaire en toute circonstance, j’aime le travail d’équipe et faire les efforts nécessaires permettant d’aboutir à un résultat efficace, propre et qualitatif !');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210613173722', '2021-06-13 17:37:56', 487),
('DoctrineMigrations\\Version20210615135112', '2021-06-15 13:52:03', 473),
('DoctrineMigrations\\Version20210805100349', '2021-08-10 14:59:47', 424);

-- --------------------------------------------------------

--
-- Structure de la table `domain`
--

CREATE TABLE `domain` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `domain`
--

INSERT INTO `domain` (`id`, `domain_name`) VALUES
(1, 'Programmation'),
(2, 'MAO'),
(3, 'Infographie'),
(4, 'Montage vidéo'),
(5, '3D'),
(6, 'Game Design'),
(7, 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE TABLE `experience` (
  `id` int(11) NOT NULL,
  `experience_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience_date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `experience`
--

INSERT INTO `experience` (`id`, `experience_name`, `experience_date`, `experience_detail`) VALUES
(1, 'AGENT DE MAINTENANCE INFORMATIQUE', '2011', 'Accueil et service aux clients, infographie, installation, réparation, dépannage de matériel à Canet Informatique'),
(2, 'AGENT D’IMPRESSION\r\n', '2012', 'Sérigraphie, impression, réalisation d’enseignes lumineuses à ALPHA\'B Perpignan'),
(3, 'ANIMATEUR PÉRISCOLAIRE', '2013', 'Encadrement d’enfants, activités pédago-ludiques à l’École primaire de Simon BOUSSIRON Perpignan'),
(4, 'FORMATION AUTODIDACTE POLYVALENTE', '2015-2017', 'Formation auto didacte apprentissage de nouvelles technologies, de nouveaux logiciels, création) et bénévolat'),
(5, 'AGENT DE SURVEILLANCE DE LA VOIE PUBLIQUE', '2017', 'Surveillance, contrôle, contact, prévention et interventions diverses à la police municipale de Canet en Roussillon'),
(6, 'MUSICIEN DANS UN GROUPE', '2017-2019', 'Démarchage, transport, utilisation et montage de matériel, sonorisation, cohésion d’équipe, organisation, création et arrangements'),
(7, 'ÉCOLE DE SOUS OFFICIER DE GENDARMERIE', '2019-2020', 'Déontologie, vie militaire, cours de contact, de sécurité routière, maîtrise de l’adversaire, armement, intervention, gestion d’un groupe, accueil du public, télécommunications, outils intranet, rédaction de comptes rendus');

-- --------------------------------------------------------

--
-- Structure de la table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `hobby_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobby_image` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `hobby`
--

INSERT INTO `hobby` (`id`, `hobby_name`, `hobby_image`) VALUES
(1, 'Sport', 'sport.png'),
(2, 'Films & séries', 'filmsseries.png'),
(3, 'Technologie', 'technologie.png'),
(4, 'Musique', 'musique.png'),
(5, 'Jeux vidéos', 'jeuxvideos.png'),
(6, 'Science', 'science.png');

-- --------------------------------------------------------

--
-- Structure de la table `quality`
--

CREATE TABLE `quality` (
  `id` int(11) NOT NULL,
  `quality_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quality`
--

INSERT INTO `quality` (`id`, `quality_name`) VALUES
(1, 'Volontaire'),
(2, 'Créatif'),
(3, 'Polyvalent'),
(4, 'Esprit d’équipe'),
(5, 'Bilingue anglais'),
(6, 'Esprit d\'analyse'),
(7, 'Rigoureux');

-- --------------------------------------------------------

--
-- Structure de la table `skill`
--

CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `parent_domain_id` int(11) NOT NULL,
  `skill_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_url` longtext COLLATE utf8mb4_unicode_ci,
  `skill_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `skill`
--

INSERT INTO `skill` (`id`, `parent_domain_id`, `skill_name`, `skill_image`, `skill_url`, `skill_detail`) VALUES
(1, 1, 'C++', 'c++.svg', 'https://fr.wikipedia.org/wiki/C%2B%2B', 'C++ est un langage de programmation compilé permettant la programmation sous de multiples paradigmes, dont la programmation procédurale, la programmation orientée objet et la programmation générique.'),
(2, 1, 'C#', 'csharp.svg', NULL, 'C# est un langage de programmation orientée objet, commercialisé par Microsoft depuis 2002 et destiné à développer sur la plateforme Microsoft .NET.'),
(3, 1, 'Javascript', 'javascript.svg', NULL, 'JavaScript est un langage de programmation de scripts principalement employé dans les pages web interactives et à ce titre est une partie essentielle des applications web. Avec les technologies HTML et CSS, JavaScript est parfois considéré comme l\'une des technologies cœur du World Wide Web.');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(1, 'adrien.masanet@hotmail.fr', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$fCJw6DBzSsELA6GB1pjcgA$em6FA982xWc3QgT/kFDi9I/yk/vN4+vgr92hFe4aNF8');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
