import Particles from "particlesjs";

window.onload = function ()
{
  Particles.init( {
    selector: '.particlesbackground',
    maxParticles: 300,
    sizeVariations: 8,
    speed: .1,
    color: "#ffffff95"
  } );
};