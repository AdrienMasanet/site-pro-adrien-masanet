import * as basicScroll from 'basicscroll'

document.querySelectorAll( ".parallaxwrapper" ).forEach( ( elem ) =>
{
  elem.style.height=document.body.offsetHeight + "px";
} )

document.querySelectorAll( '.parallax' ).forEach( ( elem ) =>
{
  elem.style.height = document.body.offsetHeight + "px";

  const modifier = elem.getAttribute( 'data-modifier' )

  basicScroll.create( {
    elem: elem,
    from: 0,
    to: document.body.clientHeight,
    direct: true,
    props: {
      '--translateY': {
        from: '0',
        to: `${ 10 * modifier }px`
      }
    }
  } ).start()

} )