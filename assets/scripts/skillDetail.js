function generateSkillDetail ( skillName, skillImage, skillDetail, skillUrl )
{
  let skillsGlobalContainer = $( ".skillsglobalcontainer" )[ 0 ];

  let skillsGlobalContainerDomRect = skillsGlobalContainer.getBoundingClientRect();

  let skillsDetailContainer = $( '<div/>', {
    class: 'skilldetail backdropblur',
    width: skillsGlobalContainerDomRect.width,
    height: skillsGlobalContainerDomRect.height,
    x: skillsGlobalContainerDomRect.x,
    y: skillsGlobalContainerDomRect.y
  } ).appendTo( skillsGlobalContainer );

  let skillsDetailNameAndDetailContainer = $( '<div/>', {
    class: 'skilldetailnameanddetailcontainer'
  } ).appendTo( skillsDetailContainer );

  let skillsDetailContainerName = $( '<h4/>', {
    text: skillName,
    class: 'skilldetailname'
  } ).appendTo( skillsDetailNameAndDetailContainer );

  let skillsDetailContainerDetailAndImage = $( '<div/>', {
    class: 'skilldetailimageanddetail'
  } ).appendTo( skillsDetailNameAndDetailContainer );

  if ( skillUrl ) {

    let skillsDetailContainerUrl = $( '<a/>', {
      href: skillUrl,
      target: '_blank',
      class: 'skilldetailimagewrapper'
    } ).appendTo( skillsDetailContainerDetailAndImage );

    let skillsDetailContainerImage = $( '<img/>', {
      src: 'build/static/skills/' + skillImage,
      alt: 'compétence sélectionnée'
    } ).appendTo( skillsDetailContainerUrl );

  } else {

    let skillsDetailContainerWrapper = $( '<div/>', {
      class: 'skilldetailimagewrapper',
    } ).appendTo( skillsDetailContainerDetailAndImage );

    let skillsDetailContainerImage = $( '<img/>', {
      src: 'build/static/skills/' + skillImage,
      alt: 'compétence sélectionnée'
    } ).appendTo( skillsDetailContainerWrapper );

  }

  let skillsDetailContainerDetail = $( '<p/>', {
    text: skillDetail,
    class: 'skilldetaildetail'
  } ).appendTo( skillsDetailContainerDetailAndImage );

  let skillsDetailContainerClose = $( '<div/>', {
    text: "Fermer",
    class: 'skilldetailclose button'
  } ).appendTo( skillsDetailContainer );

  skillsDetailContainerClose[ 0 ].addEventListener( 'click', function ()
  {
    skillsDetailContainer[ 0 ].remove();
  } );
}

$( function ()
{

  let skillsElements = document.querySelectorAll( ".skill" );

  for ( let skill of skillsElements ) {
    skill.addEventListener( 'click', function ()
    {
      generateSkillDetail(
        skill.getAttribute( "skillname" ),
        skill.getAttribute( "skillimage" ),
        skill.getAttribute( "skilldetail" ),
        skill.getAttribute( "skillurl" ),
      );
    } );
  }
} );