<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210821211109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE domain CHANGE id id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE experience CHANGE id id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE hobby CHANGE id id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE quality CHANGE id id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE skill CHANGE id id INT AUTO_INCREMENT NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE skill ADD CONSTRAINT FK_5E3DE477E014210B FOREIGN KEY (parent_domain_id) REFERENCES domain (id)');
        $this->addSql('CREATE INDEX IDX_5E3DE477E014210B ON skill (parent_domain_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE domain MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE domain DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE domain CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE experience MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE experience DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE experience CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE hobby MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE hobby DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE hobby CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE quality MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE quality DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE quality CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE skill MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE skill DROP FOREIGN KEY FK_5E3DE477E014210B');
        $this->addSql('DROP INDEX IDX_5E3DE477E014210B ON skill');
        $this->addSql('ALTER TABLE skill DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE skill CHANGE id id INT NOT NULL');
    }
}
