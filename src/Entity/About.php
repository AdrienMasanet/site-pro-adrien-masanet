<?php

namespace App\Entity;

use App\Repository\AboutRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AboutRepository::class)
 */
class About
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $aboutDetail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAboutDetail(): ?string
    {
        return $this->aboutDetail;
    }

    public function setAboutDetail(string $aboutDetail): self
    {
        $this->aboutDetail = $aboutDetail;

        return $this;
    }
}
