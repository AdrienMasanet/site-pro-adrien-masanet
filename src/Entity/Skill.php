<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 */
class Skill
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $skillName;

    /**
     * @ORM\Column(type="text")
     */
    private $skillImage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $skillUrl;

    /**
     * @ORM\ManyToOne(targetEntity=Domain::class, inversedBy="domainSkills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parentDomain;

    /**
     * @ORM\Column(type="text")
     */
    private $skillDetail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSkillName(): ?string
    {
        return $this->skillName;
    }

    public function setSkillName(string $skillName): self
    {
        $this->skillName = $skillName;

        return $this;
    }

    public function getSkillImage(): ?string
    {
        return $this->skillImage;
    }

    public function setSkillImage(string $skillImage): self
    {
        $this->skillImage = $skillImage;

        return $this;
    }

    public function getSkillUrl(): ?string
    {
        return $this->skillUrl;
    }

    public function setSkillUrl(?string $skillUrl): self
    {
        $this->skillUrl = $skillUrl;

        return $this;
    }

    public function getParentDomain(): ?Domain
    {
        return $this->parentDomain;
    }

    public function setParentDomain(?Domain $parentDomain): self
    {
        $this->parentDomain = $parentDomain;

        return $this;
    }

    public function getSkillDetail(): ?string
    {
        return $this->skillDetail;
    }

    public function setSkillDetail(string $skillDetail): self
    {
        $this->skillDetail = $skillDetail;

        return $this;
    }
}
