<?php

namespace App\Entity;

use App\Repository\IpThatCameRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IpThatCameRepository::class)
 */
class IpThatCame
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="datetime")
     */
    private $firstVisitDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $viewsCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getFirstVisitDate(): ?\DateTimeInterface
    {
        return $this->firstVisitDate;
    }

    public function setFirstVisitDate(\DateTimeInterface $firstVisitDate): self
    {
        $this->firstVisitDate = $firstVisitDate;

        return $this;
    }

    public function getViewsCount(): ?int
    {
        return $this->viewsCount;
    }

    public function setViewsCount(int $viewsCount): self
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    public function incrementViewsCount(): self
    {
        $this->viewsCount++;

        return $this;
    }
}
