<?php

namespace App\Entity;

use App\Repository\ExperienceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExperienceRepository::class)
 */
class Experience
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $experienceName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $experienceDate;

    /**
     * @ORM\Column(type="text")
     */
    private $experienceDetail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExperienceName(): ?string
    {
        return $this->experienceName;
    }

    public function setExperienceName(string $experienceName): self
    {
        $this->experienceName = $experienceName;

        return $this;
    }

    public function getExperienceDate(): ?string
    {
        return $this->experienceDate;
    }

    public function setExperienceDate(string $experienceDate): self
    {
        $this->experienceDate = $experienceDate;

        return $this;
    }

    public function getExperienceDetail(): ?string
    {
        return $this->experienceDetail;
    }

    public function setExperienceDetail(string $experienceDetail): self
    {
        $this->experienceDetail = $experienceDetail;

        return $this;
    }
}
