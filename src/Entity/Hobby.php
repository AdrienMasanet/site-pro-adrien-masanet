<?php

namespace App\Entity;

use App\Repository\HobbyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HobbyRepository::class)
 */
class Hobby
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $HobbyName;

    /**
     * @ORM\Column(type="text")
     */
    private $HobbyImage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHobbyName(): ?string
    {
        return $this->HobbyName;
    }

    public function setHobbyName(string $HobbyName): self
    {
        $this->HobbyName = $HobbyName;

        return $this;
    }

    public function getHobbyImage(): ?string
    {
        return $this->HobbyImage;
    }

    public function setHobbyImage(string $HobbyImage): self
    {
        $this->HobbyImage = $HobbyImage;

        return $this;
    }
}
