<?php

namespace App\Entity;

use App\Repository\QualityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QualityRepository::class)
 */
class Quality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $qualityName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQualityName(): ?string
    {
        return $this->qualityName;
    }

    public function setQualityName(string $qualityName): self
    {
        $this->qualityName = $qualityName;

        return $this;
    }
}
