<?php

namespace App\Entity;

use App\Repository\DomainRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DomainRepository::class)
 */
class Domain
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=50)
   */
  private $domainName;

  /**
   * @ORM\OneToMany(targetEntity=Skill::class, mappedBy="parentDomain")
   */
  private $domainSkills;

  public function __construct()
  {
    $this->domainSkills = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getDomainName(): ?string
  {
    return $this->domainName;
  }

  public function setDomainName(string $domainName): self
  {
    $this->domainName = $domainName;

    return $this;
  }

  /**
   * @return Collection|Skill[]
   */
  public function getDomainSkills(): Collection
  {
    return $this->domainSkills;
  }

  public function addDomainSkill(Skill $domainSkill): self
  {
    if (!$this->domainSkills->contains($domainSkill)) {
      $this->domainSkills[] = $domainSkill;
      $domainSkill->setParentDomain($this);
    }

    return $this;
  }

  public function removeDomainSkill(Skill $domainSkill): self
  {
    if ($this->domainSkills->removeElement($domainSkill)) {
      // set the owning side to null (unless already changed)
      if ($domainSkill->getParentDomain() === $this) {
        $domainSkill->setParentDomain(null);
      }
    }

    return $this;
  }

  public function __toString()
  {
    return (string) $this->getDomainName();
  }
}
