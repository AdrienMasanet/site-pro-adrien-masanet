<?php

namespace App\Repository;

use App\Entity\IpThatCame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IpThatCame|null find($id, $lockMode = null, $lockVersion = null)
 * @method IpThatCame|null findOneBy(array $criteria, array $orderBy = null)
 * @method IpThatCame[]    findAll()
 * @method IpThatCame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IpThatCameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IpThatCame::class);
    }

    // /**
    //  * @return IpThatCame[] Returns an array of IpThatCame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IpThatCame
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
