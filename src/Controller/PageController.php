<?php

namespace App\Controller;

// Load logic
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Form\ContactType;

// Load entities
use App\Entity\About;
use App\Entity\Domain;
use App\Entity\Experience;
use App\Entity\Quality;
use App\Entity\Hobby;
use App\Entity\IpThatCame;

class PageController extends AbstractController
{
  private $aboutRepository;
  private $domainRepository;
  private $experienceRepository;
  private $qualityRepository;
  private $hobbyRepository;
  private $ipThatCameRepository;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->aboutRepository = $entityManager->getRepository(About::class);
    $this->domainRepository = $entityManager->getRepository(Domain::class);
    $this->experienceRepository = $entityManager->getRepository(Experience::class);
    $this->qualityRepository = $entityManager->getRepository(Quality::class);
    $this->hobbyRepository = $entityManager->getRepository(Hobby::class);
    $this->ipThatCameRepository = $entityManager->getRepository(IpThatCame::class);
  }

  /**
   * @Route("/", name="accueil")
   */
  public function index(Request $request, EntityManagerInterface $manager, MailerInterface $mailer): Response
  {
    // Add the requester's ip to the IpThatCame table if it's not in there yet and increment the views count if it exists
    $requesterIp = $request->getClientIp();

    $existingRequesterIpEntry = $this->ipThatCameRepository->findOneBy(["ip" => $requesterIp]);

    if ($existingRequesterIpEntry) {
      $existingRequesterIpEntry->incrementViewsCount();
      $manager->persist($existingRequesterIpEntry);
    } else {
      $requesterIpEntry = new IpThatCame();
      $requesterIpEntry->setIp($requesterIp)
        ->setFirstVisitDate(new \DateTime('now', (new \DateTimeZone('Europe/Paris'))))
        ->setViewsCount(1);
      $manager->persist($requesterIpEntry);
    }

    $manager->flush();

    // Create the contact form
    $contactForm = $this->createForm(ContactType::class);
    $contactForm->handleRequest($request);

    if ($contactForm->isSubmitted() && $contactForm->isValid()) {
      $contactFormData = $contactForm->getData();
      $message = (new Email())->from($contactFormData['email'])
                              ->to('adrien.masanet@hotmail.fr')
                              ->subject('CONTACT DEPUIS LE SITE PRO')
                              ->text('De : '.$contactFormData['email'].\PHP_EOL.$contactFormData['message'],'text/plain');
      $mailer->send($message);

      $this->addFlash('success', 'Votre email a bien été envoyé, merci !');

      return $this->redirectToRoute('/#contactez-moi');
    }

    // Get data for all blocks
    $aboutText = $this->aboutRepository->find(1) ? htmlspecialchars_decode($this->aboutRepository->find(1)->getAboutDetail()) : "";
    $domains = $this->domainRepository->findAll();
    $experiences = $this->experienceRepository->findAll();
    $qualities = $this->qualityRepository->findAll();
    $hobbies = $this->hobbyRepository->findAll();
    $viewsCount = count($this->ipThatCameRepository->findAll());

    return $this->render('page/index.html.twig', [
      'aboutText' => $aboutText,
      'domains' => $domains,
      'experiences' => $experiences,
      'qualities' => $qualities,
      'hobbies' => $hobbies,
      'viewsCount' => $viewsCount,
      'contactForm' => $contactForm->createView(),
    ]);
  }
}
