<?php

namespace App\Controller\Admin;

use App\Entity\Experience;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class ExperienceCrudController extends AbstractCrudController
{
  public static function getEntityFqcn(): string
  {
    return Experience::class;
  }

  public function configureFields(string $pageName): iterable
  {
    return [
      TextField::new('experience_name'),
      TextField::new('experience_date'),
      TextareaField::new('experience_detail'),
    ];
  }
}
