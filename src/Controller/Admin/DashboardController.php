<?php

namespace App\Controller\Admin;

// Load logic
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

// Load entities
use App\Entity\About;
use App\Entity\Domain;
use App\Entity\Experience;
use App\Entity\Hobby;
use App\Entity\Quality;
use App\Entity\Skill;
use App\Entity\IpThatCame;

class DashboardController extends AbstractDashboardController
{
  function __construct(EntityManagerInterface $entityManager)
  {
    $this->ipThatCameRepository = $entityManager->getRepository(IpThatCame::class);
  }
  /**
   * @Route("/admin", name="admin")
   */
  public function index(): Response
  {
    $viewsCount = count($this->ipThatCameRepository->findAll());

    return $this->render('admin/dashboard.html.twig', [      
      'viewsCount' => $viewsCount,
    ]);
  }

  public function configureDashboard(): Dashboard
  {
    return Dashboard::new()
      ->setTitle('Adrien Masanet');
  }

  public function configureMenuItems(): iterable
  {
    yield MenuItem::linktoDashboard('Tableau de bord', 'fa fa-home');
    yield MenuItem::section();
    yield MenuItem::linkToCrud('À propos', 'fas fa-question-circle', About::class);
    yield MenuItem::linkToCrud('Spécialités', 'fas fa-archive', Domain::class);
    yield MenuItem::linkToCrud('Compétences', 'fas fa-brain', Skill::class);
    yield MenuItem::linkToCrud('Parcours', 'fas fa-suitcase', Experience::class);
    yield MenuItem::linkToCrud('Qualités', 'fas fa-address-card', Quality::class);
    yield MenuItem::linkToCrud('Centres d\'intérêt', 'fas fa-heart', Hobby::class);
    yield MenuItem::section();
    yield MenuItem::linkToLogout('Déconnexion', 'fas fa-door-closed');
  }
}
