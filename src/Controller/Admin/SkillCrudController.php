<?php

namespace App\Controller\Admin;

use App\Entity\Skill;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class SkillCrudController extends AbstractCrudController
{
  public static function getEntityFqcn(): string
  {
    return Skill::class;
  }

  public function configureFields(string $pageName): iterable
  {
    return [
      AssociationField::new('parentDomain')->hideOnIndex(),
      ImageField::new('skill_image')->setBasePath('/build/static/skills')
                                    ->setUploadDir('/assets/static/skills'),
      TextField::new('skill_name'),
      TextareaField::new('skill_detail'),
      UrlField::new('skill_url')->hideOnIndex(),
    ];
  }
}
