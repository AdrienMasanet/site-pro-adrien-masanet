<?php

namespace App\Controller\Admin;

use App\Entity\Hobby;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class HobbyCrudController extends AbstractCrudController
{
  public static function getEntityFqcn(): string
  {
    return Hobby::class;
  }

  public function configureFields(string $pageName): iterable
  {
    return [
      TextField::new('hobby_name'),
      ImageField::new('hobby_image')->setBasePath('/build/static/hobbies')
                                    ->setUploadDir('/assets/static/hobbies'),
    ];
  }
}
