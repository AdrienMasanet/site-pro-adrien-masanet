<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
              "label" => "Adresse email : ",
              "required" => true
            ])
            ->add('nom', TextType::class, [
              "label" => "Nom : ",
              "required" => true
            ])
            ->add('prenom', TextType::class, [
              "label" => "Prénom : ",
              "required" => true
            ])
            ->add('entreprise', TextType::class, [
              "label" => "Nom de votre entreprise : ",
              "required" => false
            ])
            ->add('message', TextareaType::class, [
              "label" => "Contenu de votre message : ",
              "required" => true,
              "attr" => ["rows" => "10"]
            ])
            ->add('submit', SubmitType::class, [
              "label" => "Envoyer le message",
              "attr" => ["class" => "button w-25"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
