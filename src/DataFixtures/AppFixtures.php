<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

  private $encoder;

  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }

  public function load(ObjectManager $manager)
  {
    // Création de l'admin
    $admin = new User();

    $encodedPassword = $this->encoder->encodePassword($admin, "admin");

    $admin->setEmail("adrien.masanet@hotmail.fr")
          ->setPassword($encodedPassword)
          ->setRoles(["ROLE_ADMIN"]);

    $manager->persist($admin);


    $manager->flush();
  }
}
